type Number = f64;

enum Operator {
    Add, 
    Substract, 
    Multiply, 
    Divide,
    Sqrt,
    Max,
}

enum Token {
    Num(Number),
    Op(Operator),
} 

struct Stack<T> {
    stack: Vec<T>,
}

impl<T> Stack<T> {
    fn new() -> Self {
        Stack { stack: Vec::new() }
    }

    fn pop(&mut self) -> Option<T> {
        self.stack.pop()
    }
    
    fn clear(&mut self) -> () { 
        self.stack.clear()
    }
        
    fn push(&mut self, item: T) {
        self.stack.push(item)
    }
    
    fn is_empty(&mut self) -> bool {
        self.stack.is_empty()
    }
}

fn build_token(x:&str) -> Result<Token, String> {
    match x {
        "+" => Ok(Token::Op(Operator::Add)),
        "-" => Ok(Token::Op(Operator::Substract)),
        "/" => Ok(Token::Op(Operator::Divide)),
        "*" => Ok(Token::Op(Operator::Multiply)),
        "SQRT" => Ok(Token::Op(Operator::Sqrt)),
        "MAX" => Ok(Token::Op(Operator::Max)),
        _ => x.parse::<Number>()
              .map(|v| Token::Num(v))
              .or_else(|_| Err(format!("could not parse token {}", x))),
    }
}

fn tokenize(input:&str) -> Result<Vec<Token>, String> {
     return input.split(' ')
                 .filter(|v| v != &"")
                 .map(|v| build_token(v))
                 .collect();
}

fn extract_two_operands(stack:&mut Stack<Number>, op:&str) -> Result<(Number, Number), String> {
    let operands = (&stack.pop(), &stack.pop());
    return match operands {
        (Some(right), Some(left)) => Ok((*left, *right)),
        (None, _) => Err(format!("Missing first operand for {}", op)),
        (_, None) => Err(format!("Missing second operand for {}", op)),
    }
}

fn validate_division((left, right):(Number, Number)) -> Result<(Number, Number), String> {
    if right == 0. {
        return Err(format!("Could not divide by 0"));
    }

    return Ok((left, right));
}

fn apply_operation(operation:&Operator, stack:&mut Stack<Number>) -> Result<Number, String> {
    match operation {
        Operator::Add => extract_two_operands(stack, "+").map(|(left,right)| left + right),
        Operator::Substract => extract_two_operands(stack, "-").map(|(left,right)| left - right),
        Operator::Multiply => extract_two_operands(stack, "*").map(|(left,right)| left * right),
        Operator::Divide => extract_two_operands(stack, "/").and_then(validate_division).map(|(left,right)| left / right),
        Operator::Sqrt => {
            let operand = stack.pop();
            match operand {
                Some(op) => {
                    let res = op.sqrt();
                    return Ok(res);
                },
                None => {
                    return Err(format!("Missing operand for SQRT"));
                },
            }
        },
        
        Operator::Max => {
            if stack.is_empty() {
                return Err(format!("Missing operands for MAX"))
            }

            let max = &stack.stack.iter().cloned().fold(-1./0.,Number::max);
            &stack.clear();
            return Ok(*max);
        },
    }
}

fn apply_calc(tokens:&[Token], stack:&mut Stack<Number>) -> Result<Number, String> {

    if tokens.is_empty() {
        return Ok(stack.pop().unwrap())
    }
 
    let token = &tokens[0];
    let remaining_tokens = &tokens[1..];

    let res = match token {
        Token::Num(n) => Ok(*n),
        Token::Op(op) => apply_operation(op, stack),
    };

    return res.and_then(|r| {
        stack.push(r);
        return apply_calc(remaining_tokens, stack);
    });
}

pub fn calc(input:&str) -> String {
    let res = tokenize(input)
    .and_then(|ts| {
        let mut stack = Stack::new();
        return apply_calc(&ts[..], &mut stack);
    })
    .map(|res| String::from(res.to_string()));

    match res {
        Ok(r) => r,
        Err(err) => err,
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_const() {
        let res = calc("101");
        assert_eq!(res, String::from("101"));
    }

    #[test]
    fn test_invalid_char() {
        assert_eq!(calc("a 1 +"), String::from("could not parse token a"));
    }

    #[test]
    fn test_ignore_whitespaces() {
        let res = calc("1    1    +     ");
        assert_eq!(res, String::from("2"));
    }

    #[test]
    fn test_invalid_number_of_operands_for_add() {
        assert_eq!(calc("1 +"), String::from("Missing second operand for +"));
        assert_eq!(calc("+"), String::from("Missing first operand for +"));
    }

    #[test]
    fn test_add() {
        let res = calc("1 1 +");
        assert_eq!(res, String::from("2"));
    }
    
    #[test]
    fn test_substract() {
        let res = calc("2 1 -");
        assert_eq!(res, String::from("1"));
    }


    #[test]
    fn test_invalid_number_of_operands_for_substract() {
        assert_eq!(calc("4 -"), String::from("Missing second operand for -"));
        assert_eq!(calc("-"), String::from("Missing first operand for -"));
    }
    
    #[test]
    fn test_multiply() {
        let res = calc("4 3 *");
        assert_eq!(res, String::from("12"));
    }


    #[test]
    fn test_invalid_number_of_operands_for_muliply() {
        assert_eq!(calc("4 *"), String::from("Missing second operand for *"));
        assert_eq!(calc("*"), String::from("Missing first operand for *"));
    }
    
    #[test]
    fn test_divide() {
        let res = calc("10 2 /");
        assert_eq!(res, String::from("5"));
    }

    #[test]
    fn test_divide_by_zero() {
        let res = calc("10 0 /");
        assert_eq!(res, String::from("Could not divide by 0"));
    }


    #[test]
    fn test_invalid_number_of_operands_for_divide() {
        assert_eq!(calc("4 /"), String::from("Missing second operand for /"));
        assert_eq!(calc("/"), String::from("Missing first operand for /"));
    }
    
    #[test]
    fn test_sqrt() {
        let res = calc("4 SQRT");
        assert_eq!(res, String::from("2"));
    }

    #[test]
    fn test_invalid_number_of_operands_for_sqrt() {
        assert_eq!(calc("SQRT"), String::from("Missing operand for SQRT"));
    }

    #[test]
    fn test_max() {
        let res = calc("4 5 1 7 9 MAX");
        assert_eq!(res, String::from("9"));
    }

    #[test]
    fn test_invalid_number_of_operands_for_max() {
        assert_eq!(calc("MAX"), String::from("Missing operands for MAX"));
    }

    #[test]
    fn test_multiple_operations() {
        let res = calc("6 3 1 MAX 2 / 6 * 46 + SQRT");
        assert_eq!(res, String::from("8"));
    }
}